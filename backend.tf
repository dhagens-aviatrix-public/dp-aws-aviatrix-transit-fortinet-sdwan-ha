terraform {
  required_version = ">= 0.12.0"

  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "ipaddr"

    workspaces {
      name = "dp-aws-aviatrix-transit-fortinet-sdwan-ha"
    }
  }
}
