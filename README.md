# dp-aws-aviatrix-transit-fortinet-sdwan-ha

!! Carefull, this lab can incur high costs fast due to the amount of instances and FortiGate trial licenses !!

![diagram](images/dp-aws-aviatrix-transit-fortinet-sdwan-ha.png)

The following variables are required:

key | value
--- | ---
aviatrix_admin_password | Controller admin password
aws_access_key | API Key for AWS
aws_secret_key | Secret belonging to above key
fortigate_password | admin password for provisioned FortiGate firewalls

The following variables are optional:

key | default | value
--- | --- | ---
aviatrix_admin_account | admin | Controller admin account
aviatrix_controller_ip |  controller.aws.avxlab.nl | Controller IP or FQDN
aws_account_name | AWS | Name of AWS account on controller
github_username | Dennizz | User to pull public keys from. To inject in instances
pub_dns_suffix | aws.avxlab.nl | DNS suffic to be used. Make sure this is set up in your route53
priv_dns_suffix | aws.avxlab.int | DNS suffic to be used. Make sure this is set up in your route53
fortios_version | 6.2.3 | Provide version number to deploy FortiGates with
ubuntu_image | ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-* | string to search for Ubuntu image

There are some more variables that can be used for changing the used CIDR ranges in the VPC's/VNET's. If you require this, look at the variables.tf file.