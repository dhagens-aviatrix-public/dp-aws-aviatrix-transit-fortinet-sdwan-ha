#Aviatrix controller vars
variable "aviatrix_admin_account" {
  type        = string
  description = "Aviatrix admin account, injected by Terraform Cloud"
  default     = "admin"
}

variable "aviatrix_admin_password" {
  type        = string
  description = "Aviatrix admin password, injected by Terraform Cloud"
}

variable "aviatrix_controller_ip" {
  description = "Aviatrix controller ip, injected by Terraform Cloud"
  type        = string
  default     = "controller.aws.avxlab.nl"
}

variable "fortigate_password" {
  type        = string
  description = "FortiGate admin password, injected by Terraform Cloud"
}

#account vars
variable "aws_account_name" {
  type    = string
  default = "AWS"
}

#AWS vars
variable "aws_access_key" {
  description = "AWS access key, injected by Terraform Cloud"
}

variable "aws_secret_key" {
  description = "AWS secret key, injected by Terraform Cloud"
}

variable "hubs" {

  type = map(object({
    transit_cidr = string,
    vpc1_cidr    = string,
    vpc2_cidr    = string,
    sdwan_cidr   = string,
    region       = string,
  }))

  default = {
    hub1 = {
      transit_cidr = "10.1.0.0/16",
      vpc1_cidr    = "10.11.0.0/16",
      vpc2_cidr    = "10.12.0.0/16",
      sdwan_cidr   = "10.101.0.0/16",
      region       = "eu-central-1",
    },
    hub2 = {
      transit_cidr = "10.2.0.0/16",
      vpc1_cidr    = "10.21.0.0/16",
      vpc2_cidr    = "10.22.0.0/16",
      sdwan_cidr   = "10.102.0.0/16",
      region       = "ap-southeast-1",
    },
    hub3 = {
      transit_cidr = "10.3.0.0/16",
      vpc1_cidr    = "10.31.0.0/16",
      vpc2_cidr    = "10.32.0.0/16",
      sdwan_cidr   = "10.103.0.0/16",
      region       = "us-east-2",
    },
  }
}

variable "branches" {

  type = map(object({
    cidr    = string,
    region  = string,
  }))

  default = {
    branch1 = {
      cidr    = "192.168.11.0/24",
      region  = "eu-west-2",
    },
    branch2 = {
      cidr    = "192.168.12.0/24",
      region  = "eu-west-3",
    },
    branch3 = {
      cidr    = "192.168.21.0/24",
      region  = "ap-south-1",
    },
    branch4 = {
      cidr    = "192.168.22.0/24",
      region  = "ap-southeast-2",
    },
    branch5 = {
      cidr    = "192.168.31.0/24",
      region  = "us-west-1",
    },
    branch6 = {
      cidr    = "192.168.32.0/24",
      region  = "us-east-1",
    },
  }
}

variable "ubuntu_image" {
  type    = string
  default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

variable "fortios_version" {
  type    = string
  default = "6.2.3" #Make sure the version is available in the Marketplace
}

#Github username to fetch user's ssh key
variable "github_username" {
  type    = string
  default = "Dennizz"
}

#DNS Suffix for creating records
variable "pub_dns_suffix" {
  type    = string
  default = "aws.avxlab.nl"
}
variable "priv_dns_suffix" {
  type    = string
  default = "aws.avxlab.int"
}

variable "vpn_pre_shared_key_seed" {
  type    = string
  default = "Daiqua1oozoonieth4roohuz8koo9hu0pohboh3pai3Oa2MuuxietaetaeNgaadae4gaiS1aipahPhes5iasacau6ooyeuthahPe"
}

