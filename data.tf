data "github_user" "user" {
  username = var.github_username
}

data "aws_route53_zone" "pub" {
  name         = var.pub_dns_suffix
  private_zone = false
}

data "aws_route53_zone" "priv" {
  name         = var.priv_dns_suffix
  private_zone = true
}