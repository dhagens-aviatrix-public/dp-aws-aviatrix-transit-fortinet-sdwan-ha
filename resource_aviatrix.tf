#Multi region transit peering

resource "aviatrix_transit_gateway_peering" "hub1-hub2" {
  transit_gateway_name1 = module.hub1.transit_gw_name
  transit_gateway_name2 = module.hub2.transit_gw_name
}


resource "aviatrix_transit_gateway_peering" "hub1-hub3" {
  transit_gateway_name1 = module.hub1.transit_gw_name
  transit_gateway_name2 = module.hub3.transit_gw_name
}


resource "aviatrix_transit_gateway_peering" "hub2-hub3" {
  transit_gateway_name1 = module.hub2.transit_gw_name
  transit_gateway_name2 = module.hub3.transit_gw_name
}
