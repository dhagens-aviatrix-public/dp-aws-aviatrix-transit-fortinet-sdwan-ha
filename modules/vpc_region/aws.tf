provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.region
}

resource "aws_key_pair" "user" {
  key_name   = "user_key"
  public_key = var.ssh_key
}

resource "aws_vpc" "vpc1" {
  cidr_block           = var.vpc1_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name                   = "vpc1${var.hub_id}-1",
  }
}

resource "aws_vpc" "vpc2" {
  cidr_block           = var.vpc2_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name                   = "vpc1${var.hub_id}-2",
  }
}

resource "aws_vpc" "sdwan" {
  cidr_block           = var.sdwan_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name                   = "sdwan",
  }
}

resource "aws_vpc_peering_connection" "sdwan_transit" {
  peer_vpc_id   = aws_vpc.sdwan.id
  vpc_id        = aviatrix_vpc.transit.vpc_id
  auto_accept   = true
}

resource "aws_subnet" "vpc1" {
  vpc_id     = aws_vpc.vpc1.id
  cidr_block = cidrsubnet(var.vpc1_cidr, 8, 0)
}

resource "aws_subnet" "vpc2" {
  vpc_id     = aws_vpc.vpc2.id
  cidr_block = cidrsubnet(var.vpc2_cidr, 8, 0)
}

resource "aws_subnet" "sdwan_a" {
  availability_zone = "${var.region}a"
  vpc_id     = aws_vpc.sdwan.id
  cidr_block = cidrsubnet(var.sdwan_cidr, 8, 1)
}

resource "aws_subnet" "sdwan_b" {
  availability_zone = "${var.region}b"
  vpc_id     = aws_vpc.sdwan.id
  cidr_block = cidrsubnet(var.sdwan_cidr, 8, 2)
}

resource "aws_internet_gateway" "vpc1" {
  vpc_id   = aws_vpc.vpc1.id
}

resource "aws_internet_gateway" "vpc2" {
  vpc_id   = aws_vpc.vpc2.id
}

resource "aws_internet_gateway" "sdwan" {
  vpc_id   = aws_vpc.sdwan.id
}

resource "aws_route" "default_vpc1" {
  route_table_id         = aws_vpc.vpc1.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.vpc1.id
}

resource "aws_route" "default_vpc2" {
  route_table_id         = aws_vpc.vpc2.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.vpc2.id
}

resource "aws_route" "default_sdwan" {
  route_table_id         = aws_vpc.sdwan.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.sdwan.id
}

resource "aws_route" "sdwan_transit" {
  route_table_id         = aws_vpc.sdwan.default_route_table_id
  destination_cidr_block = var.transit_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.sdwan_transit.id
}

resource "aws_route" "transit_sdwan" {
  route_table_id         = data.aws_route_table.transit.id
  destination_cidr_block = var.sdwan_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.sdwan_transit.id
}

resource "aws_security_group" "vpc1" {
  name   = "all_traffic"
  vpc_id = aws_vpc.vpc1.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "vpc2" {
  name   = "all_traffic"
  vpc_id = aws_vpc.vpc2.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sdwan" {
  name   = "all_traffic"
  vpc_id = aws_vpc.sdwan.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "srv1" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.user.key_name
  subnet_id                   = aws_subnet.vpc1.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.vpc1.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name                   = "srv${var.hub_id}-1"
    Auto-StartStop-Enabled = "",
  }
}

resource "aws_instance" "srv2" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.user.key_name
  subnet_id                   = aws_subnet.vpc2.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.vpc2.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name                   = "srv${var.hub_id}-2"
    Auto-StartStop-Enabled = "",
  }
}

#DNS records
resource "aws_route53_record" "srv1_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "srv${var.hub_id}-1.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv1.private_ip]
}

resource "aws_route53_record" "srv1_pub" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "srv${var.hub_id}-1.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv1.public_ip]
}

resource "aws_route53_record" "srv2_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "srv${var.hub_id}-2.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv2.private_ip]
}

resource "aws_route53_record" "srv2_pub" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "srv${var.hub_id}-2.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv2.public_ip]
}

resource "aws_route53_zone_association" "vpc1" {
  zone_id = data.aws_route53_zone.priv.zone_id
  vpc_id  = aws_vpc.vpc1.id
}

resource "aws_route53_zone_association" "vpc2" {
  zone_id = data.aws_route53_zone.priv.zone_id
  vpc_id  = aws_vpc.vpc2.id
}

resource "aws_route53_zone_association" "sdwan" {
  zone_id = data.aws_route53_zone.priv.zone_id
  vpc_id  = aws_vpc.sdwan.id
}