#AWS Bootstrap environment for FortiGate
resource "aws_s3_bucket" "bootstrap" {
  bucket = "${var.region}-dp-aws-aviatrix-fortinet-sdwan-hub${var.hub_id}"
  acl    = "private"
}

#bootstrap config files for FortiGate FW
resource "aws_s3_bucket_object" "config_a" {
  bucket = aws_s3_bucket.bootstrap.id
  key    = "hub${var.hub_id}-a_config.conf"
  source = local_file.template_a.filename
}

resource "aws_s3_bucket_object" "config_b" {
  bucket = aws_s3_bucket.bootstrap.id
  key    = "hub${var.hub_id}-b_config.conf"
  source = local_file.template_b.filename
}

locals {
  template-a = templatefile("${path.module}/bootstrap/hub-a.tpl", {
    ssh_key        = var.ssh_key,
    pre-shared-key = "${var.hub_id}${var.vpn_pre_shared_key_seed}",
    hub_id         = var.hub_id
    transit_gw     = aviatrix_transit_gateway.transit_gateway.private_ip
    transit_gw_ha  = aviatrix_transit_gateway.transit_gateway.ha_private_ip
    password       = var.fortigate_password
    }
  )
  template-b = templatefile("${path.module}/bootstrap/hub-b.tpl", {
    ssh_key        = var.ssh_key,
    pre-shared-key = "${var.hub_id}${var.vpn_pre_shared_key_seed}",
    hub_id         = var.hub_id
    transit_gw     = aviatrix_transit_gateway.transit_gateway.private_ip
    transit_gw_ha  = aviatrix_transit_gateway.transit_gateway.ha_private_ip
    password       = var.fortigate_password
    }
  )
}

resource "local_file" "template_a" {
  content  = local.template-a
  filename = "${path.module}/bootstrap/hub${var.hub_id}-a.conf"
}

resource "local_file" "template_b" {
  content  = local.template-b
  filename = "${path.module}/bootstrap/hub${var.hub_id}-b.conf"
}

resource "aws_instance" "firewall_a" {
  ami                         = data.aws_ami.fortios.id
  instance_type               = "t2.small"
  subnet_id                   = aws_subnet.sdwan_a.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.sdwan.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  iam_instance_profile = var.iam_role_name
  source_dest_check    = false
  private_ip           = "10.10${var.hub_id}.1.10"
  user_data            = <<EOF
  {
    "bucket" : "${aws_s3_bucket.bootstrap.bucket}",
    "region" : "${var.region}",
    "config" : "/${aws_s3_bucket_object.config_a.key}",
  }
  EOF

  depends_on = [
    aws_s3_bucket_object.config_a
  ]

  tags = {
    Name                   = "fgt-hub${var.hub_id}-a",
    Auto-StartStop-Enabled = "",
  }
}

resource "aws_instance" "firewall_b" {
  ami                         = data.aws_ami.fortios.id
  instance_type               = "t2.small"
  subnet_id                   = aws_subnet.sdwan_b.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.sdwan.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  iam_instance_profile = var.iam_role_name
  source_dest_check    = false
  private_ip           = "10.10${var.hub_id}.2.10"
  user_data            = <<EOF
  {
    "bucket" : "${aws_s3_bucket.bootstrap.bucket}",
    "region" : "${var.region}",
    "config" : "/${aws_s3_bucket_object.config_b.key}",
  }
  EOF

  depends_on = [
    aws_s3_bucket_object.config_b
  ]

  tags = {
    Name                   = "fgt-hub${var.hub_id}-b",
    Auto-StartStop-Enabled = "",
  }
}

resource "aws_eip" "firewall_a" {
  vpc = true
}

resource "aws_eip" "firewall_b" {
  vpc = true
}

resource "aws_eip_association" "eip_firewall_a" {
  instance_id   = aws_instance.firewall_a.id
  allocation_id = aws_eip.firewall_a.id
}

resource "aws_eip_association" "eip_firewall_b" {
  instance_id   = aws_instance.firewall_b.id
  allocation_id = aws_eip.firewall_b.id
}

resource "aws_route53_record" "fgt_a_pub" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "fgt-hub${var.hub_id}-a.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.firewall_a.public_ip]
}

resource "aws_route53_record" "fgt_a_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "fgt-hub${var.hub_id}-a.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.firewall_a.private_ip]
}

resource "aws_route53_record" "fgt_b_pub" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "fgt-hub${var.hub_id}-b.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.firewall_b.public_ip]
}

resource "aws_route53_record" "fgt_b_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "fgt-hub${var.hub_id}-b.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.firewall_b.private_ip]
}