#Transit VPC
resource "aviatrix_vpc" "transit" {
  cloud_type           = 1
  name                 = "vpc-transit-${var.region}"
  region               = var.region
  cidr                 = var.transit_cidr
  account_name         = var.aws_account_name
  aviatrix_firenet_vpc = false
  aviatrix_transit_vpc = true
}

#Transit GW
resource "aviatrix_transit_gateway" "transit_gateway" {
  enable_active_mesh = true
  cloud_type         = 1
  vpc_reg            = var.region
  gw_name            = "tg-${var.region}"
  gw_size            = "t2.micro"
  vpc_id             = aviatrix_vpc.transit.vpc_id
  subnet             = cidrsubnet(var.transit_cidr, 12, 5)
  account_name       = var.aws_account_name
  ha_subnet          = cidrsubnet(var.transit_cidr, 12, 6)
  ha_gw_size         = "t2.micro"
  connected_transit  = true
  tag_list = [
    "Auto-StartStop-Enabled:",
  ]
}

#Spoke GW VPC1
resource "aviatrix_spoke_gateway" "spoke_vpc1" {
  enable_active_mesh = true
  cloud_type         = 1
  account_name       = var.aws_account_name
  gw_name            = "spoke-gw-${var.hub_id}-vpc1"
  vpc_id             = aws_vpc.vpc1.id
  vpc_reg            = var.region
  gw_size            = "t2.micro"
  subnet             = cidrsubnet(var.vpc1_cidr, 8, 0)
  transit_gw         = aviatrix_transit_gateway.transit_gateway.gw_name
  tag_list = [
    "Auto-StartStop-Enabled:",
  ]
}

#Spoke GW VPC2
resource "aviatrix_spoke_gateway" "spoke_vpc2" {
  enable_active_mesh = true
  cloud_type         = 1
  account_name       = var.aws_account_name
  gw_name            = "spoke-gw-${var.hub_id}-vpc2"
  vpc_id             = aws_vpc.vpc2.id
  vpc_reg            = var.region
  gw_size            = "t2.micro"
  subnet             = cidrsubnet(var.vpc2_cidr, 8, 0)
  transit_gw         = aviatrix_transit_gateway.transit_gateway.gw_name
  tag_list = [
    "Auto-StartStop-Enabled:",
  ]
}

resource "aviatrix_transit_external_device_conn" "fortigate" {
  vpc_id                    = aviatrix_vpc.transit.vpc_id
  connection_name           = "FortiGate-SDWAN-${var.region}"
  gw_name                   = aviatrix_transit_gateway.transit_gateway.gw_name
  connection_type           = "bgp"
  ha_enabled                = true
  bgp_local_as_num          = "65000"
  bgp_remote_as_num         = "6500${var.hub_id}"
  backup_bgp_remote_as_num  = "6500${var.hub_id}"
  remote_gateway_ip         = aws_instance.firewall_a.private_ip
  backup_remote_gateway_ip  = aws_instance.firewall_b.private_ip
  pre_shared_key            = "${var.hub_id}${var.vpn_pre_shared_key_seed}-fg1"
  backup_pre_shared_key     = "${var.hub_id}${var.vpn_pre_shared_key_seed}-fg2"
  local_tunnel_cidr         = "172.17.${var.hub_id}1.2/30,172.17.${var.hub_id}1.6/30"
  remote_tunnel_cidr        = "172.17.${var.hub_id}1.1/30,172.17.${var.hub_id}1.5/30"
  backup_local_tunnel_cidr  = "172.17.${var.hub_id}2.2/30,172.17.${var.hub_id}2.6/30"
  backup_remote_tunnel_cidr = "172.17.${var.hub_id}2.1/30,172.17.${var.hub_id}2.5/30"
}