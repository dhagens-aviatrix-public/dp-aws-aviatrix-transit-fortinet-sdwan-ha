output "vpc_transit" {
    value = aviatrix_vpc.transit
}

output "pub_ip_a" {
    value = aws_eip.firewall_a.public_ip
}

output "pub_ip_b" {
    value = aws_eip.firewall_b.public_ip
}

output "transit_gw_name" {
    value = aviatrix_transit_gateway.transit_gateway.gw_name
}