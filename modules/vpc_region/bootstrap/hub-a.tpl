#config-version=FGTAWS-6.2.3-FW-build1066-200327:opmode=0:vdom=0:user=admin
#conf_file_ver=187393718135403
#buildno=8404
#global_vdom=1
config system global
    set hostname "hub${hub_id}-a"
    set timezone 04
    set admintimeout 60
end

config system ha
    set session-pickup enable
    set session-pickup-expectation enable
    set session-pickup-connectionless enable
    unset session-sync-dev
end

config system settings
    set auxiliary-session enable
end

config system cluster-sync
    edit 1
        set peerip 10.10${hub_id}.2.10
        set ipsec-tunnel-sync disable
end

config system admin
    edit "admin"
        set accprofile "super_admin"
        set vdom "root"
        set password "${password}"
        set ssh-public-key1 "${ssh_key}"
    next
end

config vpn ipsec phase1-interface
    edit "HUB_${hub_id}"
        set interface "port1"
        set type dynamic
        set ike-version 2
        set keylife 28800
        set peertype any
        set net-device disable
        set proposal aes256-sha256
        set add-route disable
        set auto-discovery-sender enable
        set tunnel-search nexthop
        set psksecret ${pre-shared-key}-a
        set dpd-retryinterval 5
        set dpd on-idle        
        set mode-cfg enable
        set assign-ip enable
        set assign-ip-from range
        set ipv4-start-ip 172.16.${hub_id}1.10
        set ipv4-end-ip 172.16.${hub_id}1.100
        set ipv4-netmask 255.255.255.0        
    next
    edit "HUB_${hub_id}-TG"
        set interface "port1"
        set ike-version 1
        set keylife 28800
        set peertype any
        set proposal aes256-sha256
        set add-route disable
        set psksecret "${pre-shared-key}-fg1"
        set dpd-retryinterval 5
        set remote-gw ${transit_gw}
    next
    edit "HUB_${hub_id}-TGHA"
        set interface "port1"
        set ike-version 1
        set keylife 28800
        set peertype any
        set proposal aes256-sha256
        set add-route disable
        set psksecret "${pre-shared-key}-fg1"
        set dpd-retryinterval 5
        set remote-gw ${transit_gw_ha}
    next
end

config vpn ipsec phase2-interface
    edit "HUB_${hub_id}"
        set phase1name "HUB_${hub_id}"
        set proposal aes256-sha256
        set keylifeseconds 1800
    next
    edit "HUB_${hub_id}-TG"
        set phase1name "HUB_${hub_id}-TG"
        set proposal aes256-sha256
    next
    edit "HUB_${hub_id}-TGHA"
        set phase1name "HUB_${hub_id}-TGHA"
        set proposal aes256-sha256
    next    
end

config system interface
    edit "HUB_${hub_id}"
        set vdom "root"
        set type tunnel
        set interface "port1"
        set ip 172.16.${hub_id}1.1 255.255.255.255
        set remote-ip 172.16.${hub_id}1.254 255.255.255.0
        set allowaccess ping
    next
    edit "HUB_${hub_id}-TG"
        set vdom "root"
        set type tunnel
        set interface "port1"
        set ip 172.17.${hub_id}1.1 255.255.255.255
        set remote-ip 172.17.${hub_id}1.2 255.255.255.252
        set allowaccess ping
    next
    edit "HUB_${hub_id}-TGHA"
        set vdom "root"
        set type tunnel
        set interface "port1"
        set ip 172.17.${hub_id}1.5 255.255.255.255
        set remote-ip 172.17.${hub_id}1.6 255.255.255.252
        set allowaccess ping
    next    
end

config system zone
    edit TRANSIT
        set interface HUB_${hub_id}-TG HUB_${hub_id}-TGHA port1
    end
end

config router bgp
    set as 6500${hub_id}
    set ibgp-multipath enable
    set ebgp-multipath enable
    set graceful-restart enable
    set additional-path enable
    set additional-path-select 4
    set graceful-restart-time 1
    set graceful-update-delay 1
    config network
        edit 1
            set prefix 10.10${hub_id}.1.0 255.255.255.0
        next
    end
    config neighbor
        edit "172.17.${hub_id}1.2"
            set remote-as 65000
            set soft-reconfiguration enable
        next
        edit "172.17.${hub_id}1.6"
            set remote-as 65000
            set soft-reconfiguration enable
        next
    end
    config neighbor-group
        edit VPN-PEERS
            set remote-as 6500${hub_id}
            set next-hop-self enable
            set soft-reconfiguration enable
            set link-down-failover enable
            set additional-path both
            set adv-additional-path 4
            set route-reflector-client enable
            set keep-alive-timer 5
            set holdtime-timer 15
            set advertisement-interval 1
            set bfd enable
        next
    end
    config neighbor-range
        edit 1
            set prefix 172.16.${hub_id}1.0/24
            set neighbor-group VPN-PEERS
        next
    end
end

config firewall policy
    edit 1
        set name "To-Transit"
        set srcintf "HUB_${hub_id}"
        set dstintf "TRANSIT"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
    next
    edit 2
        set name "From-Transit"
        set srcintf "TRANSIT"
        set dstintf "HUB_${hub_id}"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
    next
    edit 3
        set name "spoke2spoke"
        set srcintf "HUB_${hub_id}"
        set dstintf "HUB_${hub_id}"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
    next
end   