data "aws_route53_zone" "pub" {
  name         = var.pub_dns_suffix
  private_zone = false
}

data "aws_route53_zone" "priv" {
  name         = var.priv_dns_suffix
  private_zone = true
}

data "aws_route_table" "transit" {
  vpc_id = aviatrix_vpc.transit.vpc_id
  filter {
    name   = "tag:Name"
    values = ["${aviatrix_vpc.transit.name}-rtb2"]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = [var.ubuntu_image]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_ami" "fortios" {
  most_recent = true
  filter {
    name   = "name"
    values = ["FortiGate-VM64-AWSONDEMAND*${var.fortios_version}*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["679593333241"] # Marketplace
}
