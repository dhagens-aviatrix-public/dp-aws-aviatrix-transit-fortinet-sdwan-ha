variable "hub_id" {
    type = string
}

variable "region" {
    type = string
    default = "eu-west-1"
}

variable "transit_cidr" {
    type = string
}

variable "vpc1_cidr" {
    type = string
}

variable "vpc2_cidr" {
    type = string
}

variable "sdwan_cidr" {
    type = string
}

variable "aws_account_name" {
    type = string
}

variable "aws_access_key" {
    type = string
}

variable "aws_secret_key" {
    type = string
}

variable "fortios_version" {
    type = string
}

variable "fortigate_password" {
    type = string
}

variable "iam_role_name" {
    type = string
}

variable "ssh_key" {
    type = string
}

variable "ubuntu_image" {
    type = string
}

variable "vpn_pre_shared_key_seed" {
  type    = string
}

#DNS Suffix for creating records
variable "pub_dns_suffix" {
  type    = string
  default = "aws.avxlab.nl"
}
variable "priv_dns_suffix" {
  type    = string
  default = "aws.avxlab.int"
}