#config-version=FGTAWS-6.2.3-FW-build1066-200327:opmode=0:vdom=0:user=admin
#conf_file_ver=187393718135403
#buildno=8404
#global_vdom=1
config system global
    set hostname "b${branch_id}"
    set timezone 04
    set admintimeout 60
end

config system admin
    edit "admin"
        set accprofile "super_admin"
        set vdom "root"
        set password "${password}"
        set ssh-public-key1 "${ssh_key}"
    next
end

config system settings
    set auxiliary-session enable
end

config router bgp
    set as 6500${hub_id}
    set ibgp-multipath enable
    set graceful-restart enable
    set additional-path enable
    set additional-path-select 4
    set graceful-restart-time 1
    set graceful-update-delay 1
    config network
        edit 1
            set prefix 192.168.${hub_id}${regional_branch_nr}.0 255.255.255.0
        next
    end
    config neighbor
        edit "172.16.${hub_id}1.1"
            set remote-as 6500${hub_id}
            set route-map-out "lan"
            set bfd enable
            set additional-path both
            set adv-additional-path 4
            set soft-reconfiguration enable
            set next-hop-self enable
            set link-down-failover enable
        next  
        edit "172.16.${hub_id}2.1"
            set remote-as 6500${hub_id}
            set route-map-out "lan"
            set bfd enable
            set additional-path both
            set adv-additional-path 4
            set soft-reconfiguration enable
            set next-hop-self enable
            set link-down-failover enable
        next 
    end  
end

config router static
    edit 1
        set device port1
        set dynamic-gateway enable
    next
    edit 200
        set dst 10.0.0.0 255.0.0.0
        set blackhole enable
        unset distance
    next
    edit 201
        set dst 172.16.0.0 255.240.0.0
        set blackhole enable
        unset distance
    next
end

config vpn ipsec phase1-interface
    edit "TO_HUB${hub_id}-a"
        set interface "port1"
        set ike-version 2
        set keylife 28800
        set peertype any
        set net-device enable
        set proposal aes256-sha256
        set mode-cfg enable
        set add-route disable
        set localid "TO_HUB_A"
        set remote-gw ${hub_pub_ip_a}
        set psksecret ${pre-shared-key}-a
        set dpd-retryinterval 5
        set dpd on-demand        
        set auto-discovery-receiver enable
    next
    edit "TO_HUB${hub_id}-b"
        set interface "port1"
        set ike-version 2
        set keylife 28800
        set peertype any
        set net-device enable
        set proposal aes256-sha256
        set mode-cfg enable
        set add-route disable
        set localid "TO_HUB_B"        
        set remote-gw ${hub_pub_ip_b}
        set psksecret ${pre-shared-key}-b
        set dpd-retryinterval 5
        set dpd on-demand        
        set auto-discovery-receiver enable
    next
end

config vpn ipsec phase2-interface
    edit "TO_HUB${hub_id}-a"
        set phase1name "TO_HUB${hub_id}-a"
        set proposal aes256-sha256
        set keepalive enable
        set keylifeseconds 1800
        set auto-negotiate enable
    next
    edit "TO_HUB${hub_id}-b"
        set phase1name "TO_HUB${hub_id}-b"
        set proposal aes256-sha256
        set keepalive enable
        set keylifeseconds 1800
        set auto-negotiate enable
    next
end

config system interface
    edit "TO_HUB${hub_id}-a"
        set type tunnel
        set allowaccess ping
    next
    edit "TO_HUB${hub_id}-b"
        set type tunnel
        set allowaccess ping
    next
end

config system virtual-wan-link
    set status enable
    set load-balance-mode weight-based
    config members
        edit 1
            set interface "TO_HUB${hub_id}-a"
            set weight 100
        next
        edit 2
            set interface "TO_HUB${hub_id}-b"
            set weight 100
        next
    end
    config health-check
        edit "HUB_A"
            set server "10.10${hub_id}.1.10"
            set protocol ping
            set interval 1000
            set recoverytime 10
            set members 1
        next
        edit "HUB_B"
            set server "10.10${hub_id}.2.10"
            set protocol ping
            set interval 1000
            set recoverytime 10
            set members 2
        next
    end
    config service
        edit 1
            set name "all"
            set mode sla
            set dst "all"
            set src "all"
            set link-cost-threshold 0
            set priority-members 1 2
            set sla-compare-method number
        next
    end
end

config firewall policy
    edit 1
        set name "Any-IN"
        set srcintf "virtual-wan-link"
        set dstintf "port1"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
    edit 2
        set name "Any-OUT"
        set srcintf "port1"
        set dstintf "virtual-wan-link"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "ALL"
        set logtraffic all
    next
end




