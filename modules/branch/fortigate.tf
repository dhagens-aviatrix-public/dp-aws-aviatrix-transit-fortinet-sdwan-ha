#AWS Bootstrap environment for FortiGate
resource "aws_s3_bucket" "bootstrap" {
  bucket = "${var.region}-dp-aws-aviatrix-fortinet-sdwan-branch${var.branch_id}"
  acl    = "private"
}

#bootstrap config files for FortiGate FW
resource "aws_s3_bucket_object" "config" {
  bucket = aws_s3_bucket.bootstrap.id
  key    = "branch${var.branch_id}_config.conf"
  source = local_file.template.filename
}

locals {
  template = templatefile("${path.module}/bootstrap/branch.tpl", {
    branch_id          = var.branch_id,
    regional_branch_nr = var.regional_branch_nr,
    hub_id             = var.hub_id,
    hub_pub_ip_a       = var.hub_pub_ip_a,
    hub_pub_ip_b       = var.hub_pub_ip_b,
    pre-shared-key     = "${var.hub_id}${var.vpn_pre_shared_key_seed}",
    ssh_key            = var.ssh_key,
    password           = var.fortigate_password
    }
  )
}

resource "local_file" "template" {
  content  = local.template
  filename = "${path.module}/bootstrap/branch${var.branch_id}.conf"
}

resource "aws_instance" "firewall" {
  ami                         = data.aws_ami.fortios.id
  instance_type               = "t2.small"
  subnet_id                   = aws_subnet.lan.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.default.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  iam_instance_profile = var.iam_role_name
  source_dest_check    = false
  user_data            = <<EOF
  {
    "bucket" : "${aws_s3_bucket.bootstrap.bucket}",
    "region" : "${var.region}",
    "config" : "/${aws_s3_bucket_object.config.key}",
  }
  EOF

  depends_on = [
    aws_s3_bucket_object.config
  ]
  tags = {
    Name                   = "fgt-b${var.branch_id}",
    Auto-StartStop-Enabled = "",
  }
}

resource "aws_route53_record" "fgt_branch" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "fgt-b${var.branch_id}.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.firewall.public_ip]
}

resource "aws_route53_record" "fgt_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "fgt-b${var.branch_id}.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.firewall.private_ip]
}
