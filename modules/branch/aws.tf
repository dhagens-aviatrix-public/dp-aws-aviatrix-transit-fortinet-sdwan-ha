provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.region
}

resource "aws_key_pair" "user" {
  key_name   = "user_key"
  public_key = var.ssh_key
}

resource "aws_vpc" "default" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_subnet" "lan" {
  vpc_id     = aws_vpc.default.id
  cidr_block = var.vpc_cidr
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
}

resource "aws_route" "default" {
  route_table_id         = aws_vpc.default.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id
}

resource "aws_route" "net-10" {
  route_table_id         = aws_vpc.default.default_route_table_id
  destination_cidr_block = "10.0.0.0/8"
  instance_id            = aws_instance.firewall.id
}

resource "aws_route" "net-192" {
  route_table_id         = aws_vpc.default.default_route_table_id
  destination_cidr_block = "192.168.0.0/16"
  instance_id            = aws_instance.firewall.id
}

resource "aws_route" "net-172" {
  route_table_id         = aws_vpc.default.default_route_table_id
  destination_cidr_block = "172.16.0.0/12"
  instance_id            = aws_instance.firewall.id
}

resource "aws_security_group" "default" {
  name   = "all_traffic"
  vpc_id = aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "srv" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.user.key_name
  subnet_id                   = aws_subnet.lan.id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.default.id]
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name                   = "srv-b${var.branch_id}",
    Auto-StartStop-Enabled = "",
  }
}

#DNS records
resource "aws_route53_record" "srv_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "srv-b${var.branch_id}.${var.priv_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv.private_ip]
}

resource "aws_route53_record" "srv1_pub" {
  zone_id = data.aws_route53_zone.pub.zone_id
  name    = "srv-b${var.branch_id}.${var.pub_dns_suffix}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.srv.public_ip]
}

resource "aws_route53_zone_association" "default" {
  zone_id = data.aws_route53_zone.priv.zone_id
  vpc_id  = aws_vpc.default.id
}