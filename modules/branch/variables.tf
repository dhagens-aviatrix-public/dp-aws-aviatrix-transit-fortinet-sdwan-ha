variable "branch_id" {
    type = string
}

variable "regional_branch_nr" {
    type = string
}

variable "hub_id" {
    type = string
}

variable "hub_pub_ip_a" {
    type = string
}

variable "hub_pub_ip_b" {
    type = string
}

variable "region" {
    type = string
    default = "eu-west-1"
}

variable "vpc_cidr" {
    type = string
}

variable "ssh_key" {
    type = string
}

variable "ubuntu_image" {
    type = string
}

variable "fortios_version" {
    type = string
}

variable "fortigate_password" {
    type = string
}

variable "iam_role_name" {
    type = string
}

variable "vpn_pre_shared_key_seed" {
  type    = string
}

#AWS vars
variable "aws_access_key" {
  description = "AWS access key, injected by Terraform Cloud"
}

variable "aws_secret_key" {
  description = "AWS secret key, injected by Terraform Cloud"
}

#DNS Suffix for creating records
variable "pub_dns_suffix" {
  type    = string
  default = "aws.avxlab.nl"
}
variable "priv_dns_suffix" {
  type    = string
  default = "aws.avxlab.int"
}


